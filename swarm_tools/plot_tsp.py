import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import sys
import csv
import argparse

parser = argparse.ArgumentParser(description='')
parser.add_argument('input', nargs='*')
parser.add_argument('-t', '--title', type=str)
parser.add_argument('-s', '--show', action='store_true')
args = parser.parse_args()


for file in args.input:
    f =  open(file, 'r')
    data = csv.reader(f)
    id=[]
    x=[]
    y=[]
    for row in data:
        id.append(float(row[0]))
        x.append(float(row[1]))
        y.append(float(row[2]))

    plt.cla()

    if args.title:
        plt.title(args.title)
    else:
        plt.title("tsp")
    plt.xlabel('x')
    plt.ylabel('y')
    plt.xlim([0,max(x)+20])
    plt.ylim([0,max(y)+20])
    plt.plot(x,y,label='route', lw=2, color='r')
    plt.plot(x[0::len(x)-1][::-1],y[0::len(y)-1][::-1],label='route', lw=2, color='r', alpha=0.3)
    plt.scatter(x,y,color='r')
    for i, t in enumerate(id):
        i = int(i)
        if i%5 == 0:
            plt.annotate(t, (x[i], y[i]))
    plt.legend()
    if args.show:
        plt.show()
    else:
        plt.savefig(file.split(".")[0]+".png")
