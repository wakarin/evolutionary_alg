import sys
import csv
import argparse

parser = argparse.ArgumentParser(description='')
parser.add_argument('input', nargs='*')
parser.add_argument('-t', '--title', type=str)
parser.add_argument('-s', '--show', action='store_true')
args = parser.parse_args()

m=[]
d=[]
for file in args.input:
    f =  open(file, 'r')
    strings = f.readlines()
    x=[]
    y=[]
    for (i,j) in enumerate(strings):
        x.append(i)
        y.append(int(j))

    print(min(y), y.index(min(y)))
    m.append(min(y))
    d.append(y.index(min(y)))

avg = sum(m)/len(m)
diff = [(i-avg)**2 for i in m]
b = (sum(diff)/len(diff))**0.5


print(avg, b)
