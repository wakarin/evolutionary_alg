#include<iostream>
#include<fstream>
#include<sstream>
#include<string>
#include<cstdlib>
#include<vector>
#include<cmath>
#include<cctype>

struct city_t{
    int id;
    float x;
    float y;

    void print() {
        std::cout << "ID:"<< id << "\nx:" << x << "\ty:" << y << std::endl;
    }

    int euc_2d_dist(city_t other) {
        return (int) std::sqrt(std::pow(x-other.x, 2) + std::pow(y-other.y, 2) + 0.5);
    }
};

void write_coordinate(std::vector<city_t> ans, std::string fname);

class Tsp
{
    public:
        std::string info;
        std::vector<city_t> cities;
        std::vector<std::vector<city_t>> ansors;

        Tsp(char* tsp_file) {
            set_probrem(tsp_file);
        }

        void print() {
            std::cout << info << std::endl;
            for(city_t i: cities) {
                i.print();
            }
        }


        void result() {
            write_coordinate(ansors.back(), "ans.csv");
            std::vector<int> length;
            int max = 0;
            int min = 99999;
            int sum = 0;
            for(std::vector<city_t> i : ansors) {
                int l = calc_totaldist(i);
                length.push_back(l);
                if(min > l) min = l;
                if(max < l) max = l;
                sum+=l;
            }
            float avg = sum/length.size();
            std::cout << "min:" << min << " max:" << max << " avg:" << avg << " std_deviation:" << std_deviation(length, avg) << std::endl;

        }


    private:
         float std_deviation(std::vector<int> len, float avg){
            float t=1;
            for(int i : len) {
                t += std::pow(((float)i-avg), 2);
            }
            return std::sqrt(t/len.size());
        }

        void set_probrem(char* fname) {
            std::ifstream fin;
            fin.open(fname);
            if(!fin){
                std::cout << "Error: cannot open file" << std::endl;
                exit(1);
            }
            std::string s;
            while(getline(fin, s)){
                if(std::isdigit(s.front())){
                    std::stringstream ss{s};
                    std::string buf;
                    std::vector<float> c_buf;
                    while(getline(ss, buf, ' ')) {
                        c_buf.push_back(std::stof(buf));
                    }
                    cities.push_back({(int)c_buf[0], c_buf[1], c_buf[2]});
                }
                else{
                    info += s+"\n";
                }

            }
        }
        int calc_totaldist(std::vector<city_t> ans) {
            int ret=0;
            for(int i=0; i<ans.size()-2; i++) {
                ret += ans[i].euc_2d_dist(ans[i+1]);
            }
            return ret + ans[ans.size()-1].euc_2d_dist(ans[0]);
        }
};

void write_coordinate(std::vector<city_t> ans, std::string fname) {
    std::ofstream ofs(fname);
    for(city_t i : ans) {
        ofs << i.id << "," << i.x << "," << i.y << std::endl;
    }
    ofs.close();
}

std::vector<city_t> t_opt(std::vector<city_t> sol) {
    if(sol.size()< 4) {
        return sol;
    }
    std::vector<city_t> ret = sol;
    int dist;
    int imp_dist;
    bool T =true;
    while(T) {
        T =false;
        for(int i=0; i<sol.size()-3; i++) {
            for(int j=i+2; j<sol.size()-1; j++) {
                dist = sol[i].euc_2d_dist(sol[i+1]); + sol[j].euc_2d_dist(sol[j+1]);
                imp_dist = sol[i].euc_2d_dist(sol[j]); + sol[j+1].euc_2d_dist(sol[i+1]);
                if (imp_dist < dist) {
                    T = false;
                    if (dist -imp_dist > 60)
                        write_coordinate(sol,"b"+std::to_string(imp_dist)+"_"+std::to_string(dist)+".csv");
                    ret.clear();
                    for(int m=0; m<=i; m++) {
                        ret.push_back(sol[m]);
                    }
                    for(int m=j; m>i; m--) {
                        ret.push_back(sol[m]);
                    }
                    for(int m=j+1; m<sol.size(); m++){
                        ret.push_back(sol[m]);
                    }
                    sol = ret;
                    if (dist -imp_dist > 60)
                        write_coordinate(sol,"a"+std::to_string(imp_dist)+"_"+std::to_string(dist)+".csv");
                }
            }
        }

        for(int i=1; i<sol.size()-2; i++) {
            dist = sol[sol.size()-1].euc_2d_dist(sol[0]); + sol[i].euc_2d_dist(sol[i+1]);
            imp_dist = sol[sol.size()-1].euc_2d_dist(sol[i]); + sol[0].euc_2d_dist(sol[i+1]);
                if (imp_dist < dist) {
                    T = false;
                    if (dist -imp_dist > 60)
                        write_coordinate(sol,"b"+std::to_string(imp_dist)+"_"+std::to_string(dist)+".csv");
                    ret.clear();
                    for(int m=0; m<=i; m++) {
                        ret.push_back(sol[m]);
                    }
                    for(int m=sol.size()-1; m>i; m--) {
                        ret.push_back(sol[m]);
                    }
                    sol = ret;
                    if(dist - imp_dist > 60)
                        write_coordinate(sol,"a"+std::to_string(imp_dist)+"_"+std::to_string(dist)+".csv");
                }
        }
    }
    return ret;
}

std::vector<city_t> nn(std::vector<city_t> cities, int first_itr) {
    std::vector<city_t> ret;
//    std::vector<city_t> lst = cities;
    city_t first = cities[first_itr];
    ret.push_back(first);
    cities.erase(cities.begin()+first_itr);

    while (!cities.empty()){
        city_t target = ret.back();
        int near_itr;
        int ndist = 99999;
        for(int i=0; i<cities.size(); i++) {
            if(ndist > target.euc_2d_dist(cities[i])) {
                near_itr = i;
                ndist = target.euc_2d_dist(cities[near_itr]);
            }
        }
        ret.push_back(cities[near_itr]);
        cities.erase(cities.begin()+near_itr);
        ret = t_opt(ret);
    }
    return ret;
}

std::vector<std::vector<city_t>> nn_loop(Tsp prob) {
    std::vector<std::vector<city_t>> ret;
    for(int i=0; i<prob.cities.size(); i++) {
        ret.push_back(nn(prob.cities, i));
    }
    return ret;
}

int main(int argc, char* argv[]) {
    Tsp probrem(argv[1]);
    probrem.ansors = nn_loop(probrem);
    probrem.result();
}
