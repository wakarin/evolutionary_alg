#include"../1st/glaph.h"
#include<iostream>
#include<fstream>
#include<vector>
#include<random>
#include<algorithm>
#include<tuple>

struct solution_t {
    std::vector<int> solution;
    int violation;
    void print() {
        std::cout << "violation: " << violation << std::endl;
        std::cout << "solution" <<  std::endl;
        for(int i : solution){
            std::cout << i << " ";
        }
        std::cout << std::endl;
    }
    bool operator<(const solution_t& right) const {
        return violation == right.violation ? solution < right.solution : violation < right.violation;
    }

};

int calc_violation(int size, std::vector<std::vector<int>> glaph, std::vector<int> solution);
std::vector<solution_t> set_random_solutions(std::vector<std::vector<int>> glaph, int len, int seed, int mu);
std::vector<solution_t> mutation_solutions(std::vector<std::vector<int>> glaph, std::vector<solution_t> solutions, int len, int seed, int lambda);



int main(int argc, char* argv[]) {
    int size = 30;
    if(argv[1] != NULL) {
        size = std::stoi(argv[1]);
    }
    int seed = 123;
    if(argv[2] != NULL) {
        seed = std::stoi(argv[2]);
    }
    int mu = 60;
    if(argv[3] != NULL) {
        mu = std::stoi(argv[3]);
    }
    int lambda = 100;
    if(argv[4] != NULL) {
        lambda = std::stoi(argv[4]);
    }
    bool inc_parent = false; //loosely
    std::string s = "inc";
    if(s == argv[5]) {
        inc_parent = true; //tightly
        std::cout << "include parent" << std::endl;
    }

    bool dens = false; //loosely
    std::string S = "tight";
    if(S == argv[6]) {
        dens = true; //tightly
    }

    std::vector<int> color{0,1,2};
    std::vector< std::vector<int>> arr = make_glaph(size, dens, seed);


    std::mt19937 rd_src(seed);
    std::uniform_int_distribution<int> rd_3(color.front(),color.back());

    std::vector<solution_t> solutions;
    solutions = set_random_solutions(arr, size, seed, mu);

    std::ofstream ofs("es_log"+std::to_string(size)+"_"+std::to_string(mu)+"_"+std::to_string(lambda)+"_"+argv[5]+"_"+argv[6]+"_"+std::to_string(seed)+".csv");

    int cnt = 0;
    while(cnt < 120) {
        std::cout << "counter: " << cnt << std::endl;
        std::vector<solution_t> children;
        children = mutation_solutions(arr, solutions, size, seed, lambda);
        if(inc_parent) {
            children.insert(children.end(), solutions.begin(), solutions.end());
        }

        sort(children.begin(), children.end());

        //for(solution_t s : children) {
        //    s.print();
        //}

        int sum=0;
        for(int i=0; i<children.size(); i++) {
            sum+=children[i].violation;
        }
        ofs << cnt << "," << children.front().violation << "," <<  sum/children.size() << "," << children.back().violation << std::endl;

        children[0].print();
        if(children[0].violation==0) {
            std::cout << "es search finished!" << std::endl;
            break;
        }

        std::vector<solution_t> tmp(children.begin(), children.begin()+solutions.size());
        solutions = tmp;
        cnt++;
    }

    ofs.close();
    return 0;
}

int calc_violation(int size, std::vector<std::vector<int>> glaph, std::vector<int> solution) {
    int violation = 0;
//    std::vector<int> violator;
    for(int i=0; i<size; i++){
        for(int j=0; j<size; j++){
            if(glaph[i][j] == 1 && solution[i]==solution[j]){
                violation++;
//                violator.push_back(i);
//                violator.push_back(j);
            }
        }
    }
//    std::sort(violator.begin(), violator.end());
//    violator.erase(std::unique(violator.begin(), violator.end()), violator.end());
    return violation;
}

std::vector<solution_t> set_random_solutions(std::vector<std::vector<int>> glaph, int len, int seed, int mu) {
    std::vector<solution_t> ret;
    std::mt19937 rd(seed);
    std::uniform_int_distribution<int> rd_3(0,2);

    for(int c=0; c<mu; c++) {
        std::vector<int> arr;
        arr.resize(len);

        for(int i=0; i<len; i++) {
            arr[i] = rd_3(rd);
            std::cout << arr[i] << ' ';
        }
        solution_t sol = {arr, calc_violation(len, glaph, arr)};
        ret.push_back(sol);
    }

    return ret;
}

std::vector<solution_t> mutation_solutions(std::vector<std::vector<int>> glaph, std::vector<solution_t> solutions, int len, int seed, int lambda) {
    std::vector<solution_t> ret;
    std::mt19937 rd(seed);
    std::uniform_int_distribution<int> rd_3(0,2);
    std::uniform_int_distribution<int> rd_sols(0,solutions.size()-1);
    std::uniform_int_distribution<int> rd_sol(0,len-1);

    for(int c=0; c<lambda; c++) {
        //std::cout << c << std::endl;
        solution_t target = solutions[rd_sols(rd)];
        while(true){
            int ch = rd_3(rd);
            int tr = rd_sol(rd);
            if (target.solution[tr] != ch) {
                target.solution[tr] = ch;
                break;
            }
        }
        solution_t push = {target.solution, calc_violation(len, glaph, target.solution)};
        ret.push_back(push);
    }

    return ret;
}
