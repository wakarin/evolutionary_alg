import matplotlib.pyplot as plt
import sys
import csv
import argparse
import re

parser = argparse.ArgumentParser(description='')
parser.add_argument('-i', '--input', nargs='*')
parser.add_argument('-s', '--show', action='store_true')
args = parser.parse_args()

pat = r'([0-9]+)'

for file in args.input:
    f =  open(file, 'r')
    data = csv.reader(f)
    x=[]
    y1=[]
    y2=[]
    y3=[]
    for row in data:
        x.append(int(row[0]))
        y1.append(int(row[1]))
        y2.append(int(row[2]))
        y3.append(int(row[3]))

    print(file.split("_")[1])
    size = re.findall(pat, file.split("_")[1])[0]
    print(size)
    plt.cla()
    plt.title("Evolutionary Computation size="+size+" mu="+file.split("_")[2]+" lambda="+file.split("_")[3]+"_"+file.split("_")[4].split(".")[0])
    plt.xlabel('times')
    plt.ylabel('violation')
    plt.xlim([0,max(x)+20])
    plt.ylim([0,max(y3)+5])
    if y1[-1]==0:
        plt.plot(x,y1,label='min', lw=2, color='r')
    else:
        plt.plot(x,y1,label='min')
    plt.plot(x,y2,label='avg')
    plt.plot(x,y3,label='max')
    plt.legend()
    if args.show:
        plt.show()
    else:
        plt.savefig(file.split(".")[0]+".png")
