#include"../1st/glaph.h"
#include<iostream>
#include<fstream>
#include<vector>
#include<random>
#include<algorithm>
#include<tuple>

std::tuple <int, std::vector<int>> calc_violation(int size, std::vector<std::vector<int>> glaph, std::vector<int> solution);
std::vector<int> set_random_solution(int len, int seed);

int main(int argc, char* argv[]) {
    std::vector<int> color{0,1,2};

    int size = 30;
    if(argv[1] != NULL) {
        size = std::stoi(argv[1]);
    }
    int seed = 123;
    if(argv[2] != NULL) {
        seed = std::stoi(argv[2]);
    }
    bool dens = false; //loosely
    std::string S = "tight";
    if(argv[3] != NULL && S == argv[3]) {
        dens = true; //tightly
    }

    std::vector< std::vector<int> > arr = make_glaph(size, dens, seed);

    std::mt19937 rd_src(seed);
    std::uniform_int_distribution<int> rd_3(color.front(),color.back());

    std::vector<int> solution;
    solution = set_random_solution(size, seed);

    std::ofstream ofs("climb_log"+std::to_string(size)+"_"+argv[3]+"_"+std::to_string(seed)+".csv");

    int violation = 0;
    int child_violation = 0;
    std::vector<int> violator;
    std::vector<int> child_violator;
    std::tie(violation, violator) = calc_violation(size, arr, solution);
    int counter = 0;
    int same_counter = 0;

    while(counter < 70000) {
        //output current status
        std::cout << "counter: " << counter << std::endl;
        std::cout << "same_counter: " << same_counter << std::endl;
        std::cout << "violation: " << violation << std::endl;
        std::cout << "violator: ";
        for(int x : violator) {
            std::cout << x << ' ' ;
        }
        std::cout << std::endl << "solution: ";
        for(int x : solution) {
            std::cout << x << ' ' ;
        }
        std::cout << std::endl;

        //select change color target and shuffle insert color array
        std::shuffle(violator.begin(), violator.end(), rd_src);
        int target = violator[0];
        std::cout << "target:" << target << std::endl;
        std::shuffle(color.begin(), color.end(), rd_src);

        //search minimum violation color
        for(int c : color){
            std::vector<int> sol_tmp = solution;
            sol_tmp[target] = c;
            std::tie(child_violation,child_violator) = calc_violation(size, arr, sol_tmp);
            std::cout << c << ":" << violation << std::endl;
            if(child_violation < violation){
                solution[target] = c;
                violation = child_violation;
                violator = child_violator;
                same_counter = 0;
            }
        }

        ofs << counter << "," << violation << std::endl;

        counter++;
        same_counter++;

        if(violation==0) {
            std::cout << std::endl;
            std::cout << "climb search finished!" << std::endl;
            std::cout << "solution:" << std::endl;
            for(int x : solution) {
                std::cout << x << ' ' ;
            }
            std::cout << std::endl;
            break;
        }

        //if solution become local solution, reset solution.
        if(same_counter == 200) {
                std::cout << "reset solution" << std::endl;
                solution = set_random_solution(size, counter);

                std::tie(violation, violator) = calc_violation(size, arr, solution);
                same_counter = 0;
        }
    }

    ofs.close();
    return 0;
}

std::tuple <int, std::vector<int>> calc_violation(int size, std::vector<std::vector<int>> glaph, std::vector<int> solution) {
    int violation = 0;
    std::vector<int> violator;
    for(int i=0; i<size; i++){
        for(int j=0; j<size; j++){
            if(glaph[i][j] == 1 && solution[i]==solution[j]){
                violation++;
                violator.push_back(i);
                violator.push_back(j);
            }
        }
    }
    std::sort(violator.begin(), violator.end());
    violator.erase(std::unique(violator.begin(), violator.end()), violator.end());
    return std::forward_as_tuple(violation, violator);
}

std::vector<int> set_random_solution(int len, int seed) {
    std::mt19937 rd(seed);
    std::uniform_int_distribution<int> rd_3(0,2);
    std::vector<int> arr;
    arr.resize(len);

    for(int i=0; i<len; i++) {
        arr[i] = rd_3(rd);
        std::cout << arr[i] << ' ';
    }
    std::cout << std::endl;
    return arr;
}
