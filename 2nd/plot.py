import matplotlib.pyplot as plt
import sys
import csv


f =  open(sys.argv[1], 'r')
data = csv.reader(f)
x=[]
y=[]
for row in data:
    x.append(int(row[0]))
    y.append(int(row[1]))

plt.title('hill climbing')
plt.xlabel('times')
plt.ylabel('violation')
plt.xlim([0,max(x)+20])
plt.ylim([0,max(y)+5])
plt.plot(x,y)
plt.legend()
plt.show()

