#include<iostream>
#include<fstream>
#include<sstream>
#include<iomanip>
#include<string>
#include<random>
#include<cstdlib>
#include<vector>
#include<cmath>
#include<cctype>

struct city_t{
    int id;
    float x;
    float y;

    void print() {
        std::cout << "ID:"<< id << "\nx:" << x << "\ty:" << y << std::endl;
    }

    int euc_2d_dist(city_t other) {
        return (int) (std::sqrt(std::pow(x-other.x, 2) + std::pow(y-other.y, 2)) + 0.5f);
    }
};

std::vector<city_t> nn(std::vector<city_t> cities, int first_itr);
void write_coordinate(std::vector<city_t> ans, std::string fname);
bool have_link(std::vector<city_t> ans, int i, int j);

class Tsp
{
    public:
        std::string info;
        std::vector<city_t> cities;
        std::vector<std::vector<city_t>> ants;
        std::vector<city_t> ansor;
        int ansor_total_dist = 9999999;
        std::vector<std::vector<int>> dist;
        std::vector<std::vector<int>> sorted_dist;
        std::vector<int> total_dist;
        std::vector<std::vector<float>> pheromone;
        float max_pheromone;
        float min_pheromone;

        Tsp(char* tsp_file) {
            set_probrem(tsp_file);
            ants.resize(cities.size());
            set_dist();
            set_sorted_dist();
            total_dist.resize(cities.size());
            init_pheromone();
        }

        void print() {
            std::cout << info << std::endl;
            for(city_t i: cities) {
                i.print();
            }
        }

        void ant_print(int i) {
            for (city_t  j : ants[i]) {
                std::cout << j.id << ",";
            }
            std::cout << std::endl;
        }

        void ants_print() {
            for (std::vector<city_t> ant : ants) {
                for(city_t i : ant) {
                    std::cout << i.id << ",";
                }
            std::cout << std::endl;
            }
        }

        void d_print() {
            for(std::vector<int> i : dist) {
                for(int j : i) {
                    std::cout << std::setprecision(1) << j << ",";
                }
                std::cout << std::endl;
            }
        }

        void sd_print() {
            for(std::vector<int> i : sorted_dist) {
                for(int j : i) {
                    std::cout << std::setprecision(1) << j << ",";
                }
                std::cout << std::endl;
            }
        }

        void td_print() {
            int sum=0;
            int min=9999999;
            for(int i : total_dist){
                sum += i;
                if (min>i)min=i;
            }
            std::cout << "min:" << min << " avg:" << sum/total_dist.size() << std::endl;
        }

        void ph_print() {
            for(std::vector<float> i : pheromone) {
                for(float j : i) {
                    std::cout << std::setprecision(1) << j << ",";
                }
                std::cout << std::endl;
            }
        }

        void min_ph_print() {
            float min =1;
            for(std::vector<float> i : pheromone) {
                for(float j : i) {
                    if(min>j) min=j;
                }
                std::cout << "min:" << min << std::endl;
            }
        }

        void t_ph_print() {
            float sum = 0;
            for(std::vector<float> i : pheromone) {
                for(float j : i) {
                    sum += j;
                }
            }
            std::cout << sum << std::endl;
        }

        void result(std::string s) {
            std::cout << "####result####" << std::endl;
            write_coordinate(ansor, s);
            std::cout << "length: " << ansor_total_dist << std::endl;
        }

        void update_total_dist() {
            for(int i=0; i<ants.size(); i++){
                total_dist[i] = calc_totaldist(ants[i]);
            }
        }

        void update_ansor() {
            for(int i=0; i<total_dist.size(); i++){
                if(ansor_total_dist > total_dist[i]) {
                    ansor = ants[i];
                }
            }
            ansor_total_dist = calc_totaldist(ansor);
        }

        void update_mm_pheromone() {
            float rho = 0.5;
            calc_min_max_pheromone(rho);
            for(int i=0; i<cities.size(); i++) {
                for(int j=0; j<cities.size(); j++) {
                    float delta = 0;
                    if(have_link(ansor, i, j)) {
                        delta += (float) 1/ansor_total_dist;
                    }
                    float new_ph = (1-rho)*pheromone[i][j] + delta/2;
                    if(new_ph < min_pheromone){
                        pheromone[i][j] = min_pheromone;
                    }
                    else if(max_pheromone < new_ph) {
                        pheromone[i][j] = max_pheromone;
                    }
                    else{
                        pheromone[i][j] = new_ph;
                    }
                }
            }
        }

        void calc_min_max_pheromone(float rho) {
            max_pheromone = (float)1/(rho * ansor_total_dist);
            float p_best = 0.05;
            float p_dec = std::pow(p_best, (float)1/cities.size());
            min_pheromone = ((1-p_dec) / (cities.size()/2 -1)*p_dec) * max_pheromone;
        }


        void update_mm_pheromone(std::vector<city_t> best) {
          float rho = 0.5;
          for(int i=0; i<cities.size(); i++) {
              for(int j=0; j<cities.size(); j++) {
                  float delta = 0;
                  if(have_link(best, i, j)) {
                      delta += (float) 1/calc_totaldist(best);
                  }
                  pheromone[i][j] = (1-rho)*pheromone[i][j] + delta/2;
              }
          }
        }

        void clear_ants() {
          ants.clear();
          ants.resize(cities.size());
        }


    private:
        void set_dist() {
            for(city_t i : cities) {
                std::vector<int> s;
                for(city_t j : cities) {
                    s.push_back(i.euc_2d_dist(j));
                }
                dist.push_back(s);
            }
        }

        void set_sorted_dist() {
            for(std::vector<int> s : dist) {
                std::vector<int> r;
                while(true){
                    int min = 99999;
                    int itr =0;
                    for(int i=0; i< s.size(); i++){
                        if(s[i] >= 0 && min > s[i]) {
                            min = s[i];
                            itr = i;
                        }
                    }
                    if(min == 99999)break;
                    r.push_back(itr);
                    s[itr] = -1;
                }
                sorted_dist.push_back(r);
            }
        }


        float std_deviation(std::vector<int> len, float avg){
            float t=1;
            for(int i : len) {
                t += std::pow(((float)i-avg), 2);
            }
            return std::sqrt(t/len.size());
        }

        void set_probrem(char* fname) {
            std::ifstream fin;
            fin.open(fname);
            if(!fin){
                std::cout << "Error: cannot open file" << std::endl;
                exit(1);
            }
            std::string s;
            while(getline(fin, s)){
                if(std::isdigit(s.front())){
                    std::stringstream ss{s};
                    std::string buf;
                    std::vector<float> c_buf;
                    while(getline(ss, buf, ' ')) {
                        c_buf.push_back(std::stof(buf));
                    }
                    cities.push_back({(int)c_buf[0], c_buf[1], c_buf[2]});
                }
                else{
                    info += s+"\n";
                }

            }
        }
        void init_pheromone() {
            std::vector<city_t> c_nn = nn(cities, 0);
            int c_dist = calc_totaldist(c_nn);
            std::vector<float> a(cities.size(), (float)ants.size()/c_dist);
            std::vector<std::vector<float>> ret(cities.size(), a);
            pheromone = ret;
        }

        int calc_totaldist(std::vector<city_t> ans) {
            int ret=0;
            for(int i=0; i<ans.size()-1; i++) {
                ret += ans[i].euc_2d_dist(ans[i+1]);
            }
            return ret + ans[ans.size()].euc_2d_dist(ans[0]);
        }
};

bool have_link(std::vector<city_t> ans, int i, int j){
    for(int k=1; k<ans.size()-1; k++){
        if(ans[k].id-1 == i){
            if((ans[k-1].id-1 == j) || (ans[k+1].id-1 == j)){
                return true;
            }
        }
        else if(ans[k].id-1 == j){
            if(ans[k-1].id-1 == i ||  ans[k+1].id-1 == i){
                return true;
            }
        }
    }
    return false;
}

void write_coordinate(std::vector<city_t> ans, std::string fname) {
    std::ofstream ofs(fname);
    for(city_t i : ans) {
        ofs << i.id << "," << i.x << "," << i.y << std::endl;
    }
    ofs.close();
}

std::vector<city_t> nn(std::vector<city_t> cities, int first_itr) {
    std::vector<city_t> ret;
    city_t first = cities[first_itr];
    ret.push_back(first);
    cities.erase(cities.begin()+first_itr);

    while (!cities.empty()){
        city_t target = ret.back();
        int near_itr;
        int ndist = 99999;
        for(int i=0; i<cities.size(); i++) {
            if(ndist > target.euc_2d_dist(cities[i])) {
                near_itr = i;
                ndist = target.euc_2d_dist(cities[near_itr]);
            }
        }
        ret.push_back(cities[near_itr]);
        cities.erase(cities.begin()+near_itr);
    }
    return ret;
}

int select_city(int now, std::vector<int> unreached, std::vector<std::vector<float>> pheromone, std::vector<std::vector<int>> dist, std::vector<std::vector<int>> sorted_dist, int a, int b){
    double div = 0;
    for(int i : unreached){
//        std::cout << i << ", "<<  now << ", " << std::pow((double)1/dist[now][i] , b) << std::endl;
//        std::cout <<"div:" << div << std::endl; //for ch130bug node id 87
        div += (double)std::pow(pheromone[now][i] , a) * std::pow((double)1/dist[now][i] , b);
    }

    std::vector<int> sort_unreached;
    for(int i=0; i<sorted_dist[now].size(); i++){
        for(int j : unreached) {
            if(sorted_dist[now][i] == j){
                sort_unreached.push_back(j);
            }
        }
    }

    std::random_device seed_gen;
    std::mt19937 rd(seed_gen());
    std::uniform_int_distribution<int> rd_s(0,9999);
    double darts = rd_s(rd) * div / 10000;

    for(int i : sort_unreached) {
        darts -= (double) std::pow(pheromone[now][i] , a) * std::pow((double) 1/dist[now][i] , b);
        if(darts <=0) {
            return i;
        }
    }
    return -1;
}


int main(int argc, char* argv[]) {
    int seed = 123;
    if(argv[2] != NULL) {
        seed = std::stoi(argv[2]);
    }
    int b = 2;
    if(argv[3] != NULL) {
        b = std::stoi(argv[3]);
    }

    Tsp probrem(argv[1]);



    std::string fname = "mmas_log_"+std::string(argv[1]).substr(4,4)+"_"+std::to_string(b)+"_"+std::to_string(seed)+".csv";
    std::string frname = "mmas_route_log_"+std::string(argv[1]).substr(4,4)+"_"+std::to_string(b)+"_"+std::to_string(seed)+".csv";
    std::ofstream of(fname);

    int cnt=0;
    int best=999999;
    while(true) {
        for (int i=0; i<probrem.ants.size(); i++){
            int now = i;
            std::vector<int> unreached;
            for(int k=0; k<probrem.cities.size(); k++) {
                if(k != now) {
                    unreached.push_back(k);
                }
            }
            probrem.ants[i].push_back(probrem.cities[now]);
            while(!unreached.empty()){
                int nearest = select_city(now, unreached, probrem.pheromone, probrem.dist, probrem.sorted_dist, 1, b);
                probrem.ants[i].push_back(probrem.cities[nearest]);
                now = nearest;
                for(int m=0; m<unreached.size(); m++){
                    if(unreached[m]==nearest){
                        unreached.erase(unreached.begin()+m);
                    }
                }
            }
        }
        probrem.update_total_dist();
        probrem.update_ansor();
        probrem.update_mm_pheromone();

//        probrem.ants_print();
        probrem.ph_print();
        probrem.td_print();


        int b=9999999;
        for(int i=0; i<probrem.total_dist.size(); i++){
            if(best > probrem.total_dist[i]) {
                best = probrem.total_dist[i];
                probrem.ansor = probrem.ants[i];
            }
            if(b > probrem.total_dist[i]) {
                b=probrem.total_dist[i];
            }
        }
        of << b << std::endl;

        cnt++;
        if(cnt == 100) break;
        probrem.clear_ants();
    }
    probrem.result(frname);
    of.close();
}
