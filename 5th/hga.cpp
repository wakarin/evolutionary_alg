#include"../1st/glaph.h"
#include<iostream>
#include<fstream>
#include<vector>
#include<random>
#include<algorithm>
#include<tuple>

struct solution_t {
    std::vector<int> solution;
    int violation;
    float fitness;
    float scale_fitness;
    void print() {
          std::cout << "violation: " << violation << std::endl;
          std::cout << "fitness: " << fitness << std::endl;
          std::cout << "scale_fitness: " << scale_fitness << std::endl;
        std::cout << "solution" <<  std::endl;
        for(int i : solution){
            std::cout << i << " ";
        }
        std::cout << std::endl;
    }
    bool operator<(const solution_t& right) const {
        return fitness == right.fitness ? solution > right.solution : fitness > right.fitness;
    }

};

int calc_violation(std::vector<std::vector<int>> glaph, std::vector<int> solution);
std::vector<int> calc_violator(std::vector<std::vector<int>> glaph, std::vector<int> solution);
std::vector<solution_t> set_random_solutions(std::vector<std::vector<int>> glaph, int seed, int num_agent);
std::vector<solution_t> mutation_solutions(std::vector<std::vector<int>> glaph, std::vector<solution_t> solutions, int seed, float p );
float calc_fitness(std::vector<std::vector<int>> glaph, solution_t solution, bool dens);
std::vector<solution_t> set_scale_sort_fitness(std::vector<std::vector<int>> glaph, bool dens, std::vector<solution_t> solutions, int scaler);


solution_t roulette_select(std::vector<solution_t> solutions,int seed);
solution_t coplulation(solution_t p1 , solution_t p2, std::vector<int> mask);
std::vector<solution_t> dairankou(std::vector<solution_t> parents, std::vector<int> mask, int num, int seed);
void print_result(std::vector<solution_t> solutions);
std::vector<int> climb(std::vector<std::vector<int>> glaph,std::vector<int> solution);

int main(int argc, char* argv[]) {
    int size = 30;
    if(argv[1] != NULL) {
        size = std::stoi(argv[1]);
    }
    int seed = 123;
    if(argv[2] != NULL) {
        seed = std::stoi(argv[2]);
    }
    int num_agent = 200;
    if(argv[3] != NULL) {
        num_agent = std::stoi(argv[3]);
    }
    int scaler = 0;
    if(argv[4] != NULL) {
        scaler = std::stoi(argv[4]);
    }
    float p = 0.05;
    if(argv[5] != NULL) {
        p = std::stof(argv[5]);
    }
    bool dens = false; //loosely
    std::string S = "tight";
    if(argv[6] != NULL && S == argv[6]) {
        dens = true; //tightly
    }

    std::vector< std::vector<int>> arr = make_glaph(size, dens, seed);

    std::mt19937 rd(seed);
    std::uniform_int_distribution<int> rd_2(0,1);
    std::vector<int> mask;
    for(int i =0; i<size; i++){
        mask.push_back(rd_2(rd));
    }

    std::vector<solution_t> solutions;
    solutions = set_random_solutions(arr, seed, num_agent);
    solutions = set_scale_sort_fitness(arr, dens, solutions, scaler);

    std::ofstream ofs("hga_log"+std::to_string(size)+"_"+std::to_string(num_agent)+"_"+std::to_string(scaler)+"_"+std::to_string(p)+"_"+argv[6]+"_"+std::to_string(seed)+".csv");

    std::vector<solution_t> tmp;
    tmp = solutions;

    int cnt = 0;
    while(cnt < 350) {
        std::cout << "counter: " << cnt << std::endl;

        solutions = tmp;
        solutions = set_scale_sort_fitness(arr, dens, solutions, scaler);
      std::cout << "#############best################" << std::endl;
        solutions[0].print();
        std::cout << "#################################" << std::endl;
        print_result(solutions);

        if(solutions[0].fitness==1) {
            std::cout << "ga search finished!" << std::endl;
            break;
        }

        std::vector<solution_t> tmep =dairankou(solutions, mask, num_agent, seed);
        solutions = tmep;

        solutions = mutation_solutions(arr, solutions, seed+cnt, p);

        for(solution_t sol : solutions) {
            int counter = 0;
            while(counter<1){
                sol.solution = climb(arr, sol.solution);
                counter++;
            }
        }

        solutions = set_scale_sort_fitness(arr, dens, solutions, scaler);
        tmp = solutions;

        float sum = 0;
        for (solution_t i : solutions) sum += i.fitness;
        ofs << solutions.front().fitness << ", " << sum/solutions.size() << ", " << solutions.back().fitness << std::endl;

        cnt++;
    }

    ofs.close();
    return 0;
}


int calc_violation(std::vector<std::vector<int>> glaph, std::vector<int> solution) {
    int violation = 0;
    for(int i=0; i<glaph.size(); i++){
        for(int j=0; j<glaph.size(); j++){
            if(glaph[i][j] == 1 && solution[i]==solution[j]){
                violation++;
            }
        }
    }
    return violation;
}

std::vector<int> calc_violator(std::vector<std::vector<int>> glaph, std::vector<int> solution) {
    std::vector<int> violator;
    for(int i=0; i<glaph.size(); i++){
        for(int j=0; j<glaph.size(); j++){
            if(glaph[i][j] == 1 && solution[i]==solution[j]){
                violator.push_back(i);
                violator.push_back(j);
            }
        }
    }
    return violator;
}

std::vector<solution_t> set_random_solutions(std::vector<std::vector<int>> glaph, int seed, int num_agent) {
    std::vector<solution_t> ret;
    std::mt19937 rd(seed);
    std::uniform_int_distribution<int> rd_3(0,2);

    for(int c=0; c<num_agent; c++) {
        std::vector<int> s;
        s.resize(glaph.size());


        for(int i=0; i<glaph.size(); i++) {
            s[i] = rd_3(rd);
        }

        solution_t sol = {s, calc_violation(glaph, s), 0, 0};
        ret.push_back(sol);
    }

    return ret;
}

std::vector<solution_t> mutation_solutions(std::vector<std::vector<int>> glaph, std::vector<solution_t> solutions, int seed, float p) {
    std::mt19937 rd(seed);
    std::uniform_int_distribution<int> rd_3(0, 2);
    std::uniform_int_distribution<int> rd_sols(0, solutions.size()-1);
    std::uniform_int_distribution<int> rd_sol(0, glaph.size()-1);

    int itr = solutions.size() * p;
    for(int c=0; c<itr; c++) {
        int rdm = rd_sols(rd);
        solution_t target = solutions[rdm];
        while(true){
            int ch = rd_3(rd);
            int tr = rd_sol(rd);
            if (target.solution[tr] != ch) {
                solutions[rdm].solution[tr] = ch;
                break;
            }
        }
    }
    return solutions;
}

float calc_fitness(std::vector<std::vector<int>> glaph, solution_t solution, bool dens) {
    int n = glaph.size();
    float num_link = 3*n; //loosely
    if (dens) {
        num_link = n*(n-1)/4; //tightly
    }
    return 1-(solution.violation/num_link);
}

std::vector<solution_t> set_scale_sort_fitness(std::vector<std::vector<int>> glaph, bool dens, std::vector<solution_t> solutions, int scaler) {
    for(int i =0; i != solutions.size(); i++) {
        solutions[i].violation = calc_violation(glaph, solutions[i].solution);
        solutions[i].fitness = calc_fitness(glaph, solutions[i], dens);
    }
    sort(solutions.begin(), solutions.end());
    if(scaler == 0) { //if scaler is 0, scaleing become liner
        std::cout << "#########" << std::endl;
        float fit_max = solutions.front().fitness;
        float fit_min = solutions.back().fitness;
        float a = fit_min / (fit_max - fit_min);
        float b = 1 / (fit_max - fit_min);
        for(int i =0; i != solutions.size(); i++) {
            solutions[i].scale_fitness = a + b*solutions[i].fitness;
        }
    }
    else { //if scale > 0, scale scale-th power
        for(int i =0; i != solutions.size(); i++) {
            solutions[i].scale_fitness = std::pow(solutions[i].fitness, scaler);
        }
    }
    return solutions;
}

solution_t roulette_select(std::vector<solution_t> solutions,int seed) {
    float sum_fit=0;
    for(solution_t i : solutions) {
        sum_fit += i.scale_fitness;
    }
    std::mt19937 rd(seed);
    std::uniform_int_distribution<int> rd_f(0,sum_fit);
    int darts = rd_f(rd);
    for(solution_t s : solutions) {
        darts -= s.scale_fitness;
        if(darts <= 0) {
            return s;
        }
    }
    return (solution_t) {};
}

solution_t coplulation(solution_t p1 , solution_t p2, std::vector<int> mask) {
    std::vector<int> s;
    for(int i=0; i<mask.size(); i++) {
        if (mask[i]) {
            s.push_back(p1.solution[i]);
        }
        else{
            s.push_back(p2.solution[i]);
        }
    }
    solution_t ret ={s, 0, 0, 0};
    return ret;
}

std::vector<solution_t> dairankou(std::vector<solution_t> solutions, std::vector<int> mask, int num, int seed) {
    std::vector<solution_t> ret;
    for(int n=0;n<num ; n++) {
        solution_t a = roulette_select(solutions, seed) ;
        solution_t b = roulette_select(solutions, seed+n) ;
        ret.push_back(coplulation(a, b, mask));
//        ret.push_back(coplulation(parents[rd_p(rd)], parents[rd_p(rd)], mask));
//        for(int i=0; i<parents.size(); i++) {
//            for(int j=i+1; j<parents.size(); j++) {
//                ret.push_back(coplulation(parents[i], parents[j], mask));
//            }
//        }
    }
    return ret;
}

void print_result(std::vector<solution_t> solutions) {
    float sum = 0;
    for (solution_t i : solutions){
        sum += i.fitness;
    }
    std::cout << solutions.front().fitness << ", " << sum/solutions.size() << ", " << solutions.back().fitness << std::endl;
}

std::vector<int> climb(std::vector<std::vector<int>> glaph, std::vector<int> solution) {
    int violation = calc_violation(glaph, solution);
    std::vector<int> violator = calc_violator(glaph, solution);
    std::mt19937 rd_src(123);
    std::shuffle(violator.begin(), violator.end(), rd_src);
    int target = violator[0];
    for(int c; c<3; c++) {
        std::vector<int> sol_tmp = solution;
        sol_tmp[target] = c;
        int c_violation = calc_violation(glaph, sol_tmp);
       // c_violator = calc_violator(glaph, sol_tmp);
        if(c_violation < violation){
            solution[target] = c;
            break;
        }
    }
    return solution;
}

