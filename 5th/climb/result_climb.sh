#!/bin/zsh
echo "%%%% result %%%"
for i in 60 90 120 150
    do
        echo "######### $i ##########"; wc -l *${i}_${1}* | grep -v 'total' | awk '{print $1}'
    done

echo "%%%% avg %%%"

for i in 60 90 120 150
    do
        echo "######### $i ##########"; wc -l *${i}_${1}* | grep -v 'total' | grep -v " 70000 " | awk '{s += $1} END {print NR,s/NR}'
    done

