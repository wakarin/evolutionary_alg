#!/bin/zsh
echo "%%%% result %%%"
for i in 60 90 120 150
    do
        echo "######### $i ##########"; wc -l *${i}_100_600_inc_${1}* | grep -v 'total' | awk '{print $1}'
    done

echo "%%%% avg %%%"

for i in 60 90 120 150
    do
        echo "######### $i ##########"; wc -l *${i}_100_600_inc_${1}* | grep -v 'total' | grep -v  " 120 " | awk '{s += $1} END {print NR,600*s/NR}'
    done

