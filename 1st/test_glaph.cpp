#include"glaph.h"
#include<iostream>
#include<vector>
#include<random>

void print_glaph(std::vector< std::vector<int> > arr, int size) {
    int space = size / 3;
    for(int i=0; i<size; i++){
        if(i%space == 0)std::cout << std::endl;
        for(int j=0; j<size; j++){
            if(j%space == 0)std::cout << " ";
            std:: cout << arr[j][i];
        }
        std:: cout << std::endl;
    }
}

int main() {
    std::cout << "test make_glaph function" << std::endl;
    int size = 30;
    std::vector< std::vector<int> >tiny_arr = make_glaph(size, true, 123);
    std::vector< std::vector<int> >loose_arr = make_glaph(size, false, 123);
    std::cout << "tiny_arr" << std::endl;
    print_glaph(tiny_arr, size);
    std::cout << "loose_arr" << std::endl;
    print_glaph(loose_arr, size);
    return 0;
}
