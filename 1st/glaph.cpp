#include"glaph.h"
#include<iostream>
#include<vector>
#include<random>

std::vector< std::vector<int> > make_glaph(int size, bool density, int seed) {
    std::vector< std::vector<int> > arr;
    arr.resize(size);
    for(int i=0; i<size; i++){
        arr[i].resize(size);
    }

    int connect_num = 0;
    if(density) {
        std::cout << "tightly" << std::endl;
        connect_num = size*(size-1)/4; //tightly
    }
    else {
        std::cout << "loosely" << std::endl;
        connect_num = size*3; //loosely
    }

    std::cout << connect_num << std::endl;

    if(connect_num > (size/3)*(size/3)*3) {
        std::cout << "connection number overflow. please up glaph size"<< std::endl;
        exit(0);
    }

    std::mt19937 rd_src(seed);
    std::uniform_int_distribution<int> rd(0,size-1);

    for(int i=0; i<connect_num ; i++) {
//        std::cout << i<< std::endl;
        int x = rd(rd_src);
        int y = rd(rd_src);
        if((x>=size/3&&y<size/3 || x>=size*2/3&&y<size*2/3) && arr[x][y] != 1){
//            std:: cout << "do " << x << "," <<  y << std::endl;
            arr[x][y] = 1;
        }else{
//           std:: cout << "undo" << std::endl;
            i--;
        }
    }

    return arr;
}

/*
int main() {
    int size = 9;
    std::vector< std::vector<int> >arr = make_glaph(size, true);
    for(int i=0; i<size; i++){
        for(int j=0; j<size; j++){
            std:: cout << arr[i][j];
        }
        std:: cout << std::endl;
    }
    return 0;
}
*/
