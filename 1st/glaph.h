#ifndef MAKE_GLAPH_H
#define MAKE_GLAPH_H
#include<vector>
extern std::vector< std::vector<int> > make_glaph(int size, bool density, int seed);
#endif
