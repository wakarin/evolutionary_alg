import matplotlib.pyplot as plt
import sys
import csv
import argparse
import re

parser = argparse.ArgumentParser(description='')
parser.add_argument('input', nargs='*')
parser.add_argument('-s', '--show', action='store_true')
args = parser.parse_args()


for file in args.input:
    f =  open(file, 'r')
    data = csv.reader(f)
    x=[]
    y1=[]
    y2=[]
    y3=[]
    for (i, row) in enumerate(data):
        x.append(i)
        y1.append(float(row[0]))
        y2.append(float(row[1]))
        y3.append(float(row[2]))

    plt.cla()
    plt.title("Genetic Algorithms ="+'.'.join(file.split(".")[0:2]))
    plt.xlabel('times')
    plt.ylabel('fitness')
    plt.xlim([0,max(x)])
    plt.ylim([0,1])
    if y1[-1]==0:
        plt.plot(x,y1,label='min', lw=2, color='r')
    else:
        plt.plot(x,y1,label='min')
    plt.plot(x,y2,label='avg')
    plt.plot(x,y3,label='max')
    plt.legend()
    if args.show:
        plt.show()
    else:
        plt.savefig('.'.join(file.split(".")[0:2])+".png")
