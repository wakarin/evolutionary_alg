import sys
import csv
import argparse
import re

parser = argparse.ArgumentParser(description='')
parser.add_argument('input', nargs='*')
parser.add_argument('-s', '--show', action='store_true')
args = parser.parse_args()


for file in args.input:
    f =  open(file, 'r')
    data = csv.reader(f)
    x=[]
    y1=[]
    y2=[]
    y3=[]
    for (i, row) in enumerate(data):
        x.append(i)
        y1.append(float(row[0]))
        y2.append(float(row[1]))
        y3.append(float(row[2]))

    print(file+", "+str(max(y3)))
